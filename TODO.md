- [x] Implementar endpoint POST(InclusaoDeVenda).
  - [x] Implementar testes unitários para POST.
    - [x] Verificar se esta sendo enviado um Item e um Vendedor.
    - [x] Verificar se a inclusão de venda possui pelo menos um Item.
    - [x] Verificar se o carrinho não está vazio.
    - [x] Salvar a nova venda.

- [x] Implementar endpoint PUT
  - [x] Implementar testes unitários para PUT
    - [x] Procurar venda por Id
  - [x] Verificar se a atualização é válida.
  - [x] Atualizar a o status da venda.

- [x] Implementar serviço para atualização de status da venda
  - [x] Aguardando pagamento -> Pagamento Aprovado
  - [x] Aguardando pagamento -> Cancelada
  - [x] Pagamento aprovado -> Enviado para transportadora
  - [x] Pagamento aprovado -> Cancelada
  - [x] Enviado para transportadora -> Entregue