﻿namespace PaymentApi.API.Helpers;

public static class BadResponseMessage
{
    public static Object BadRequestMessage(string message)
    {
        return new 
        {
            title = "Bad Request",
            status = 400,
            message = message
        };
    }
    public static Object NotFoundMessage(string message)
    {
        return new
        {
            title = "Not Found",
            status = 404,
            message = message
        };
    }
}
