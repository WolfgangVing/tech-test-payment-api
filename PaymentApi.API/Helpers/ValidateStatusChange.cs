﻿using Microsoft.AspNetCore.Mvc;
using PaymentApi.Services.Enums;

namespace PaymentApi.API.Helpers;

public static class ValidateStatusChange
{
    public static bool IsStatusChangeValid(Status statusToBeUpdated, Status newStatus)
    {
        if (statusToBeUpdated == Status.AguardandoPagamento &&
            !(newStatus == Status.PagamentoAprovado || newStatus == Status.Cancelado))
        {
            return true;
        }
        else if (statusToBeUpdated == Status.PagamentoAprovado &&
            !(newStatus == Status.EnviadoParaTransportadora || newStatus == Status.Cancelado))
        {
            return true;
        }
        else if (statusToBeUpdated == Status.EnviadoParaTransportadora && newStatus != Status.Entregue)
        {
            return true;
        }
        return false;
    }
}
