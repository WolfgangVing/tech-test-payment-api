using LiteDB;
using Microsoft.AspNetCore.Mvc;
using PaymentApi.API.Helpers;
using PaymentApi.API.Model;
using PaymentApi.Services.Enums;
using PaymentApi.Services.Interfaces;
using PaymentApi.Services.Models;
using System.ComponentModel.DataAnnotations;

namespace PaymentApi.API.Controllers;

[ApiController]
[Route("[controller]")]
public class SaleController : ControllerBase
{
    private readonly ISaleService<LiteDatabase> _saleService;

    public SaleController(ISaleService<LiteDatabase> saleService) {
        _saleService = saleService;
    }


    [HttpGet("api/{id}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    public async Task<IActionResult> GetSaleById(string id) {
        try {
            Sale saleById = await _saleService.GetSaleByIdService(id);
            if (saleById == null) 
                return NotFound(BadResponseMessage.NotFoundMessage("Nenhuma venda com este id foi encontrado."));
            
            SaleBusinessModel saleBusinessModal = new(saleById);

            return Ok(saleBusinessModal);

        } catch (Exception) {
            throw;
        }
    }


    [HttpPost("api")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> PostSale(PostSaleRequestModel postRequest) {
        if (postRequest.cart.Count == 0)
            {
                return BadRequest(BadResponseMessage.BadRequestMessage("Cart esta vazio."));
            }

        foreach (Item item in postRequest.cart)
        {
            if (item.Amount <= 0)
                return BadRequest(BadResponseMessage.BadRequestMessage("Um ou mais itens no cart esta zerado."));
        }

        try {
            SaleBusinessModel postVenda = new(await _saleService.PostSaleService(
                salesman: postRequest.salesman,
                cart: postRequest.cart));

            return CreatedAtAction(nameof(GetSaleById), new { id = postVenda.SaleId! }, postVenda);

        }
        catch (Exception) { 
            throw; 
        }
    }

    [HttpPut("api/{id}/{novoStatus}")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> UpdateVendaStatus(string id, Status novoStatus)
    {
        Sale sale = await _saleService.GetSaleByIdService(id);
        
        //Should a sale not be found, returns 404 Not Found.
        if (sale == null)
        {
            return NotFound(BadResponseMessage.NotFoundMessage("N�o encontrado venda com este Id"));
        }

        //Should a Sale have status Entregue or Cancelador should returns 400 Bad Request,
        // since it cant be updated.
        if (sale.Status == Status.Entregue || sale.Status == Status.Cancelado)
        {
            return BadRequest(
                BadResponseMessage.BadRequestMessage(
                    $"Essa venda j� foi {sale.Status.ToString().ToLower()}"));
        }

        //Validate the status change, in case it's fail returns 400 Bad Request.
        if(sale.Status == novoStatus)
        {
            return BadRequest(BadResponseMessage.BadRequestMessage("Novo status � igual ao anterior."));
        }
        else if (ValidateStatusChange.IsStatusChangeValid(sale.Status, novoStatus))
        {
            return BadRequest(BadResponseMessage.BadRequestMessage("Opera��o de atualiza��o inv�lida."));
        }

        try
        {
            sale.Status = novoStatus;

            bool performedUpdate = await _saleService.PutSaleService(sale);

            SaleBusinessModel saleUpdated = new(sale);
            return CreatedAtAction(nameof(GetSaleById), new { id }, saleUpdated);

        } 
        catch (Exception e) 
        {
            return BadRequest();
        }
    }
}