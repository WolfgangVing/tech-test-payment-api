﻿using PaymentApi.Services.Models;

namespace PaymentApi.API.Model;

public class SaleBusinessModel
{
    private readonly string? _saleId;
    private readonly Salesman _salesman;
    private readonly List<Item> _cart;
    private readonly DateTime _dataDaVenda;
    private readonly string _status;

    public SaleBusinessModel(Sale sale)
    {
        _saleId = sale.SaleId.ToString();
        _salesman = sale.Salesman!;
        _cart = sale.Cart!;
        _dataDaVenda = sale.SalesDate;
        _status = sale.Status.ToString();
    }

    public string? SaleId => _saleId;
    public Salesman? Salesman { get => _salesman; }
    public List<Item> Carrinho { get => _cart; }
    public string SalesDate { get => _dataDaVenda.ToString("dd/MM/yyyy - HH:mm:ss"); }
    public string Status { get => _status; }
}
