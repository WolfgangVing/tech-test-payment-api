﻿using Microsoft.AspNetCore.Mvc;
using PaymentApi.Services.Enums;
using System.ComponentModel.DataAnnotations;

namespace PaymentApi.API.Model
{
    [BindProperties]
    public class PutSaleRequestModel
    {
        [Required]
        public string? SaleId { get; set; }
        [Required]
        public Status Status { get; set; }
    }
}