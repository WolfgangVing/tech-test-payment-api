﻿using Microsoft.AspNetCore.Mvc;
using PaymentApi.Services.Models;
using System.ComponentModel.DataAnnotations;

namespace PaymentApi.API.Model
{
    [BindProperties]
    public class PostSaleRequestModel
    {
        [Required]
        public Salesman salesman { get; set; }

        [Required]
        public List<Item> cart { get; set; }
    }
}