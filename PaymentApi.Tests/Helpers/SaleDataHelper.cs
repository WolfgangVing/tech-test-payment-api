﻿using LiteDB;
using PaymentApi.Services.Enums;
using PaymentApi.Services.Fixtures;
using PaymentApi.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaymentApi.Tests.Helpers;

internal static class SaleDataHelper
{
    public static Sale SaleForTest => new Sale
    {
        SaleId = new ObjectId("507f1f55bcf96cd799438110"),
        SalesDate = new DateTime(2022, 08, 31),
        Status = Status.AguardandoPagamento,
        Salesman = new Salesman()
        {
            SalesmanId = 1,
            Cpf = "88888888888",
            Email = "teste@test.com",
            Name = "Testando",
            Phone = "88888888888"
        },
        Cart = new() {
                new() {
                    ItemId = 1,
                    Amount = 10
                }
            }
    };
    public static Sale SalesStatusUpdated = new Sale
        {
            SaleId = new ObjectId("507f1f55bcf96cd799438110"),
            SalesDate = new DateTime(2022, 08, 31),
            Status = Status.PagamentoAprovado,
            Salesman = new Salesman()
    {
        SalesmanId = 1,
                Cpf = "88888888888",
                Email = "teste@test.com",
                Name = "Testando",
                Phone = "88888888888"
            },
            Cart = new () {
                new () {
                    ItemId = 1,
                    Amount = 10
                }
            }
        };
    public static Sale SaleForTestCase2 = new Sale
    {
        SaleId = new ObjectId("76c78935c0a3a86a0989ef25"),
        SalesDate = new DateTime(2021, 12, 18),
        Status = Status.PagamentoAprovado,
        Salesman = new Salesman()
        {
            SalesmanId = 1,
            Cpf = "88888888888",
            Email = "teste@test.com",
            Name = "Testando",
            Phone = "88888888888"
        },
        Cart = new() {
                new () {
                    ItemId = 1,
                    Amount = 10
                }
            }
    };

    public static Sale SaleForTestCase3 = new Sale
    {
        SaleId = new ObjectId("5362352564045bd6558e997c"),
        SalesDate = new DateTime(2022, 06, 09),
        Status = Status.EnviadoParaTransportadora,
        Salesman = new Salesman()
        {
            SalesmanId = 1,
            Cpf = "88888888888",
            Email = "teste@test.com",
            Name = "Testando",
            Phone = "88888888888"
        },
        Cart = new() {
                new () {
                    ItemId = 1,
                    Amount = 10
                }
            }
    };

    internal static Sale SaleForTestCase4() => new Sale
    {
        SaleId = new ObjectId("c08b8046d761a9e553eeafb6"),
        SalesDate = new DateTime(2022, 04, 01),
        Status = Status.Cancelado,
        Salesman = new Salesman()
        {
            SalesmanId = 1,
            Cpf = "88888888888",
            Email = "teste@test.com",
            Name = "Testando",
            Phone = "88888888888"
        },
        Cart = new() {
                new() {
                    ItemId = 1,
                    Amount = 10
                }
            }
    };
}
