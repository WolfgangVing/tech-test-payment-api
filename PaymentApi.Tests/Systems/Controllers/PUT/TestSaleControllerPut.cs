﻿using PaymentApi.API.Controllers;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using PaymentApi.Services.Interfaces;
using PaymentApi.Services.Models;
using Moq;
using PaymentApi.Services.Enums;
using LiteDB;
using PaymentApi.API.Model;
using PaymentApi.Tests.Helpers;

namespace PaymentApi.Tests.Systems.Controllers.PUT;

/// <summary>
/// For the three theory test, each one is using different Data with fixed Id, so that it can test a full round
/// of possibilities for fail.
/// </summary>
public class TestSaleControllerPut
{
    [Fact]
    public async Task Put_OnSuccess_ReturnsStatusCode201()
    {
        //Arrange
        var mockVendaService = new Mock<ISaleService<LiteDatabase>>();
        PutSaleRequestModel novoStatusModel = new PutSaleRequestModel()
        {
            SaleId = "507f1f55bcf96cd799438110",
            Status = Status.PagamentoAprovado,
        };

        mockVendaService
            .Setup(service => service.GetSaleByIdService(novoStatusModel.SaleId))
            .ReturnsAsync(SaleDataHelper.SaleForTest);

        mockVendaService
            .Setup(service => service.PutSaleService(SaleDataHelper.SaleForTest))
            .ReturnsAsync(true);

        var sut = new SaleController(mockVendaService.Object);

        //Act
        var result = (CreatedAtActionResult)await sut.UpdateVendaStatus(
            novoStatusModel.SaleId!, 
            Status.PagamentoAprovado);

        //Assert
        result.StatusCode.Should().Be(201);
    }

    [Fact]
    public async Task Put_OnIdNotFound_ReturnsStatusCode404()
    {
        //Arrange
        var mockVendaService = new Mock<ISaleService<LiteDatabase>>();
        PutSaleRequestModel novoStatusModel = new PutSaleRequestModel()
        {
            SaleId = "507f1f55bcf96cd799438789",
            Status = Status.PagamentoAprovado,
        };


        mockVendaService
        .Setup(service => service.GetSaleByIdService(novoStatusModel.SaleId))
        .ReturnsAsync(It.IsAny<Sale>);

        mockVendaService
            .Setup(service => service.PutSaleService(SaleDataHelper.SaleForTest))
            .ReturnsAsync(It.IsAny<bool>());

        var sut = new SaleController(mockVendaService.Object);

        //Act
        var result = await sut.UpdateVendaStatus(
            "507f1f55bcf96cd799438789",
            Status.PagamentoAprovado);

        //Assert
        result.Should().BeOfType<NotFoundObjectResult>();
        var objectResult = (NotFoundObjectResult)result;
        objectResult.StatusCode.Should().Be(404);
    }

    /// <summary>
    /// With a fixed data which has a Status of AguardandoPagamento, this tests if the validation
    /// returns BadRequest with statuscode 400 in case it fails.
    /// There a two type of validation, the first is if the status to update 
    /// is equal to the data, the second is a invalid transition of status which shouldn't occurs.
    /// For this scenary can only transit from 1 -> 2 or 1 -> 0
    /// </summary>
    [Theory]
    [InlineData(Status.AguardandoPagamento)]
    [InlineData(Status.EnviadoParaTransportadora)]
    [InlineData(Status.Entregue)]
    public async Task Put_OnValidationOfChangeFailCase1_ReturnsBadRequest400(Status status)
    {
        //Arrange
        var mockVendaService = new Mock<ISaleService<LiteDatabase>>();
        PutSaleRequestModel novoStatusModel = new PutSaleRequestModel()
        {
            SaleId = "507f1f55bcf96cd799438110",
            Status = status,
        };

        mockVendaService
            .Setup(service => service.GetSaleByIdService(novoStatusModel.SaleId))
            .ReturnsAsync(SaleDataHelper.SaleForTest);

        mockVendaService
            .Setup(service => service.PutSaleService(SaleDataHelper.SalesStatusUpdated))
            .ReturnsAsync(It.IsAny<bool>);

        var sut = new SaleController(mockVendaService.Object);

        //Act
        var result = await sut.UpdateVendaStatus(novoStatusModel.SaleId, status);

        //Assert
        result.Should().BeOfType<BadRequestObjectResult>();
        var objectResult = (BadRequestObjectResult)result;
        objectResult.StatusCode.Should().Be(400);
    }

    /// <summary>
    /// Since the test above verify for equal status, further test, include this, will have only the case
    /// for invalid transition.
    /// For this can only transit from 2 -> 0 or 2 -> 3
    /// </summary>
    [Theory]
    [InlineData(Status.Entregue)]
    [InlineData(Status.AguardandoPagamento)]

    public async Task Put_OnValidationOfChangeFailCase2_ReturnsBadRequest400(Status status)
    {
        //Arrange
        var mockVendaService = new Mock<ISaleService<LiteDatabase>>();
        PutSaleRequestModel novoStatusModel = new PutSaleRequestModel()
        {
            SaleId = "76c78935c0a3a86a0989ef25",
            Status = status,
        };

        mockVendaService
            .Setup(service => service.GetSaleByIdService(novoStatusModel.SaleId))
            .ReturnsAsync(SaleDataHelper.SaleForTestCase2);

        var sut = new SaleController(mockVendaService.Object);

        //Act
        var result = await sut.UpdateVendaStatus("76c78935c0a3a86a0989ef25", status);

        //Assert
        result.Should().BeOfType<BadRequestObjectResult>();
        var objectResult = (BadRequestObjectResult)result;
        objectResult.StatusCode.Should().Be(400);
    }

    /// <summary>
    /// Since the test above verify for equal status, further test, include this, will have only the case
    /// for invalid transition.
    /// For this can only transit from 3 -> 4
    /// </summary>
    [Theory]
    [InlineData(Status.PagamentoAprovado)]
    [InlineData(Status.AguardandoPagamento)]
    [InlineData(Status.Cancelado)]

    public async Task Put_OnValidationOfChangeFailCase3_ReturnsBadRequest400(Status status)
    {
        //Arrange
        var mockVendaService = new Mock<ISaleService<LiteDatabase>>();
        PutSaleRequestModel novoStatusModel = new PutSaleRequestModel()
        {
            SaleId = "5362352564045bd6558e997c",
            Status = status,
        };

        mockVendaService
            .Setup(service => service.GetSaleByIdService(novoStatusModel.SaleId))
            .ReturnsAsync(SaleDataHelper.SaleForTestCase3);


        var sut = new SaleController(mockVendaService.Object);

        //Act
        var result = await sut.UpdateVendaStatus("5362352564045bd6558e997c", status);

        //Assert
        result.Should().BeOfType<BadRequestObjectResult>();
        var objectResult = (BadRequestObjectResult)result;
        objectResult.StatusCode.Should().Be(400);
    }

    /// <summary>
    /// Any Sale with Status Cancelado or Entregue shouldn't be able to perform any update.
    /// </summary>
    [Theory]
    [InlineData(Status.AguardandoPagamento)]
    [InlineData(Status.PagamentoAprovado)]
    [InlineData(Status.EnviadoParaTransportadora)]
    [InlineData(Status.Entregue)]
    [InlineData(Status.Cancelado)]

    public async Task Put_OnValidationOfChangeFailCase4_ReturnsBadRequest400(Status status)
    {
        //Arrange
        var mockVendaService = new Mock<ISaleService<LiteDatabase>>();
        PutSaleRequestModel novoStatusModel = new PutSaleRequestModel()
        {
            SaleId = "c08b8046d761a9e553eeafb6",
            Status = status,
        };

        mockVendaService
            .Setup(service => service.GetSaleByIdService(novoStatusModel.SaleId))
            .ReturnsAsync(SaleDataHelper.SaleForTestCase4);


        var sut = new SaleController(mockVendaService.Object);

        //Act
        var result = await sut.UpdateVendaStatus(novoStatusModel.SaleId, status);

        //Assert
        result.Should().BeOfType<BadRequestObjectResult>();
        var objectResult = (BadRequestObjectResult)result;
        objectResult.StatusCode.Should().Be(400);
    }
}
