using PaymentApi.API.Controllers;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using PaymentApi.Services.Interfaces;
using PaymentApi.Services.Models;
using PaymentApi.Services.Fixtures;
using Moq;
using LiteDB;
using PaymentApi.API.Model;
using PaymentApi.Tests.Helpers;

namespace PaymentApi.Tests.Systems.Controllers.GET;

public class TestSaleControllerGetById
{
    SalesFixture VendasSample = new SalesFixture();
    [Fact]
    public async Task Get_VendaById_OnSuccess_ReturnsStatusCode200OK()
    {
        //Arrange
        var mockVendaService = new Mock<ISaleService<LiteDatabase>>();

        mockVendaService
            .Setup(service => service.GetSaleByIdService("507f1f55bcf96cd799438110"))
            .ReturnsAsync(SaleDataHelper.SaleForTest);

        var sut = new SaleController(mockVendaService.Object);

        //Act
        var result = (OkObjectResult)await sut.GetSaleById("507f1f55bcf96cd799438110");

        //Assert
        result.StatusCode.Should().Be(200);
    }

    /** Test if the service, which would make use of EntityFramework, has been called at least once.   
    */
    [Fact]
    public async Task Get_OnSuccess_InvokesVendaServiceOnce()
    {
        //Arrange
        var mockVendaService = new Mock<ISaleService<LiteDatabase>>();
        Sale newSale = new Sale()
        {
            SaleId = ObjectId.NewObjectId()
        };
        string saleId = newSale.ToString()!;

        mockVendaService
            .Setup(service => service.GetSaleByIdService(saleId))
            .ReturnsAsync(It.IsAny<Sale>());

        var sut = new SaleController(mockVendaService.Object);

        //Act
        var result = await sut.GetSaleById(saleId);

        //Assert
        mockVendaService.Verify(
            service => service.GetSaleByIdService(saleId),
            Times.Once()
        );
    }

    [Fact]
    public async Task Get_OnSuccess_ReturnsAnSaleBusinessModel()
    {
        //Arrange
        var mockVendaService = new Mock<ISaleService<LiteDatabase>>();
        Sale newSale = new Sale()
        {
            SaleId = ObjectId.NewObjectId()
        };
        string saleId = newSale.ToString()!;

        mockVendaService
            .Setup(service => service.GetSaleByIdService("507f1f55bcf96cd799438110"))
            .ReturnsAsync(SaleDataHelper.SaleForTest);

        var sut = new SaleController(mockVendaService.Object);

        //Act
        var result = await sut.GetSaleById("507f1f55bcf96cd799438110");

        //Assert
        result.Should().BeOfType<OkObjectResult>();
        var objectResult = (OkObjectResult)result;
        objectResult.Value.Should().BeOfType<SaleBusinessModel>();
    }

    [Fact]
    /**
        Feito!!!
    */
    public async Task Get_OnSuccess_ReturnsOneVendaById()
    {
        //Arrange
        var mockVendaService = new Mock<ISaleService<LiteDatabase>>();
        var sale = SaleDataHelper.SaleForTest;
        var saleBuss = new SaleBusinessModel(sale);
        mockVendaService
            .Setup(service => service.GetSaleByIdService("507f1f55bcf96cd799438110"))
            .ReturnsAsync(sale);

        var sut = new SaleController(mockVendaService.Object);

        //Act
        var result = (OkObjectResult)await sut.GetSaleById("507f1f55bcf96cd799438110");

        //Assert
        result.Value.Should().BeEquivalentTo<SaleBusinessModel>(saleBuss);

    }


    /**
        FEITO!!!
    */
    [Fact]
    public async Task Get_OnNotVendaByIdFound_Returns404()
    {
        //Arrange
        var mockVendaService = new Mock<ISaleService<LiteDatabase>>();

        mockVendaService
            .Setup(service => service.GetSaleByIdService("507f1a55bcf96cd799231978"))
            .ReturnsAsync(It.IsAny<Sale>());

        var sut = new SaleController(mockVendaService.Object);

        //Act
        var result = await sut.GetSaleById("507f1a55bcf96cd799231978");

        //Assert
        result.Should().BeOfType<NotFoundObjectResult>();
    }

    
}