﻿using PaymentApi.API.Controllers;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using PaymentApi.Services.Interfaces;
using PaymentApi.Services.Models;
using Moq;
using PaymentApi.Services.Fixtures;
using LiteDB;
using PaymentApi.API.Model;
using PaymentApi.Services.Enums;

namespace PaymentApi.Tests.Systems.Controllers.POST;

public class TestSaleControllerPost
{
    [Fact]
    public async Task Post_Venda_OnSuccess_ReturnsStatusCode200OK() {
        //Arrange
        var mockVendaService = new Mock<ISaleService<LiteDatabase>>();

        PostSaleRequestModel newSale = new PostSaleRequestModel()
        {
            cart = CartFixture.Cart,
            salesman = SalesmanFixture.Salesman
        };

        Sale sale = new Sale()
        {
            SaleId = ObjectId.NewObjectId(),
            Cart = newSale.cart,
            Salesman = newSale.salesman,
            SalesDate = DateTime.Now,
            Status = Status.AguardandoPagamento
        };

        mockVendaService
            .Setup(service => service.PostSaleService(newSale.salesman, newSale.cart))
            .ReturnsAsync(sale);

        var sut = new SaleController(mockVendaService.Object);

        //Act
        var result = await sut.PostSale(newSale);
        var objectResult = (CreatedAtActionResult)result;

        //Assert
        objectResult.StatusCode.Should().Be(201);
    }

    [Fact]
    public async Task Post_OnSuccess_InvokesVendaServiceOnce()
    {
        //Arrange
        var mockVendaService = new Mock<ISaleService<LiteDatabase>>();
        PostSaleRequestModel newSale = new PostSaleRequestModel() {
            cart = CartFixture.Cart,
            salesman = SalesmanFixture.Salesman
        };

        Sale sale = new Sale()
        {
            SaleId = ObjectId.NewObjectId(),
            Cart = newSale.cart,
            Salesman = newSale.salesman,
            SalesDate = DateTime.Now,
            Status = Status.AguardandoPagamento
        };

        mockVendaService
            .Setup(service => service.PostSaleService(newSale.salesman, newSale.cart))
            .ReturnsAsync(sale);

        var sut = new SaleController(mockVendaService.Object);

        //Act
        var result = await sut.PostSale(newSale);

        //Assert
        mockVendaService.Verify(
            service => service.PostSaleService(newSale.salesman, newSale.cart),
            Times.Once()
        );
    }

    [Fact]
    public async Task Post_OnCartSemItem_ReturnsStatusCode400()
    {
        //Arrange
        var mockVendaService = new Mock<ISaleService<LiteDatabase>>();
        PostSaleRequestModel newSale = new PostSaleRequestModel()
        {
            cart = CartFixture.CartWithoutItem,
            salesman = SalesmanFixture.Salesman
        };

        mockVendaService
            .Setup(service => service.PostSaleService(newSale.salesman, newSale.cart))
            .ReturnsAsync(It.IsAny<Sale>());

        var sut = new SaleController(mockVendaService.Object);
        
        //Act
        var result = (BadRequestObjectResult)await sut.PostSale(newSale);

        //Assert

        result.StatusCode.Should().Be(400);
    }
    [Fact]
    public async Task Post_OnCartWithAItemsAmountZero_ReturnsStatusCode400()
    {
        //Arrange
        var mockVendaService = new Mock<ISaleService<LiteDatabase>>();
        PostSaleRequestModel newSale = new PostSaleRequestModel()
        {
            cart = CartFixture.CartWithAItemsAmountZero,
            salesman = SalesmanFixture.Salesman
        };

        mockVendaService
            .Setup(service => service.PostSaleService(newSale.salesman, newSale.cart))
            .ReturnsAsync(It.IsAny<Sale>());

        var sut = new SaleController(mockVendaService.Object);
        
        //Act
        var result = (BadRequestObjectResult)await sut.PostSale(newSale);

        //Assert

        result.StatusCode.Should().Be(400);
    }

    //TODO: FEITO!!!

    [Fact]
    public async Task Post_OnSuccess_ReturnsSaleBusinessResponse() {
        //Arrange
        var mockVendaService = new Mock<ISaleService<LiteDatabase>>();
        
        PostSaleRequestModel newSale = new PostSaleRequestModel()
        {
            cart = CartFixture.Cart,
            salesman = SalesmanFixture.Salesman
        };
        Sale sale = new Sale()
        {
            SaleId = ObjectId.NewObjectId(),
            Cart = newSale.cart,
            Salesman = newSale.salesman,
            SalesDate = DateTime.Now,
            Status = Status.AguardandoPagamento
        };

        mockVendaService
            .Setup(service => service.PostSaleService(newSale.salesman, newSale.cart))
            .ReturnsAsync(sale);

        var sut = new SaleController(mockVendaService.Object);

        //Act
        var result = await sut.PostSale(newSale);

        //Assert
        result.Should().BeOfType<CreatedAtActionResult>();
        var objectResult = (CreatedAtActionResult)result;
        objectResult.Value.Should().BeOfType<SaleBusinessModel>();
    }

}
