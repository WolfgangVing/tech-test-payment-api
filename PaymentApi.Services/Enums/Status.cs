namespace PaymentApi.Services.Enums;

public enum Status
{
    AguardandoPagamento = 1,
    PagamentoAprovado = 2,
    Cancelado = 0,
    EnviadoParaTransportadora = 3,
    Entregue = 4
}