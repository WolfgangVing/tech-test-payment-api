using System.ComponentModel.DataAnnotations;

namespace PaymentApi.Services.Models;


public class Salesman {
    [Required]
    public int SalesmanId { get; set; }
    [Required]
    public string? Name { get; set; }
    [Required]
    public string Cpf { get; set; }
    public string Email { get; set; }
    public string Phone { get; set; }
}