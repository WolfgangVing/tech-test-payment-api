using LiteDB;
using PaymentApi.Services.Enums;

namespace PaymentApi.Services.Models;

public class Sale {
    public ObjectId SaleId { get; set; }
    public Salesman? Salesman { get; set; }
    public List<Item> Cart { get; set; }
    public DateTime SalesDate { get; set; }
    public Status Status { get; set; }
}