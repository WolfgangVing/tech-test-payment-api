namespace PaymentApi.Services.Models;

public class Item {
    public int ItemId { get; set; }
    public int Amount { get; set; }
}