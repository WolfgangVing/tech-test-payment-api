using System;
using System.IO;
using LiteDB;
using PaymentApi.Services.Enums;
using PaymentApi.Services.Fixtures;
using PaymentApi.Services.Interfaces;
using PaymentApi.Services.Models;

namespace PaymentApi.Services;
//This folder would be used to create services in which Entity Framework would do most of the job, making our project more organized. 



public class SaleService : ISaleService<LiteDatabase> {

    public SaleService()
    {        
    }

    public async Task<Sale?> GetSaleByIdService(string id) {
        //L� nosso banco de dados
        LiteDatabase db = handleLiteDBOpenRead();
        
        try
        {
            //L� collection de com um entidade Sale de nome vendas
            ILiteCollection<Sale> collection = db.GetCollection<Sale>("sales");

            Sale doc = collection.FindById(new ObjectId(id));

            if (doc == null)
            {
                db.Dispose();
                return null;
            }

            db.Dispose();
            return doc;
        } 
        catch (Exception)
        {
            db.Dispose();
            return null;
        }
    }

    public async Task<Sale> PostSaleService(Salesman salesman, List<Item> cart) {
        //L� nosso banco de dados
        LiteDatabase db = handleLiteDBOpenRead();

        //L� collection de com um entidade Sale de nome vendas
        ILiteCollection<Sale> collection = db.GetCollection<Sale>("sales");

        Sale doc = new Sale() {
            SaleId = ObjectId.NewObjectId(),
            Salesman = salesman,
            Cart = cart,
            SalesDate = DateTime.Now,
            Status = Status.AguardandoPagamento
        };
        collection.Insert(doc);

        db.Dispose();

        return doc;
    }


    //FEITO!!!
    public async Task<bool>? PutSaleService(Sale newSaleStatus) {
        LiteDatabase db = handleLiteDBOpenRead();

        try
        {

            ILiteCollection<Sale> collection = db.GetCollection<Sale>("sales");

            bool performedUpdate = collection.Update(newSaleStatus);

            db.Dispose();

            return performedUpdate;
        } catch (Exception)
        {
            db.Dispose();
            return false;
        }
        
    }
     
    public LiteDatabase handleLiteDBOpenRead ()
    {
        //Ir� ler ou criar uma um VendaData.db neste diret�rio independente do path do computador
        LiteDatabase? db = new LiteDatabase(
            Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).FullName, @"LiteDatabase\SalesData.db").ToString()
            );

        //ler ou criar uma collection de venda
        ILiteCollection<Sale> collection = db.GetCollection<Sale>("sales");

        //creates the first sample data if doesn't exist.
        if (collection.Count() == 0)
        {

            List<Sale> saleFixture = new SalesFixture().Sales;
            collection.InsertBulk(saleFixture);


        }

        return db;
    }
}