﻿using PaymentApi.Services.Models;

namespace PaymentApi.Services.Fixtures;

public static class CartFixture
{
    public static List<Item> Cart => new List<Item>() {
        new Item()
        {
            ItemId = 15,
            Amount = 1
        }
    };

    public static List<Item> CartWithoutItem => new List<Item>() {

    };

    public static List<Item> CartWithAItemsAmountZero => new List<Item>() {
        new Item() 
        {
            ItemId = 0,
            Amount = 0
        }
    };
}
