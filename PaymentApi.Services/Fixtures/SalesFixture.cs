using LiteDB;
using PaymentApi.Services.Models;

namespace PaymentApi.Services.Fixtures;

public class SalesFixture
{
    public List<Sale> Sales;
    public SalesFixture()
    {
        Sales = new List<Sale>()
        {
        new Sale {
            SaleId = new ObjectId("646c7ca1e25b9f68cb5ef4e3"),
            SalesDate = new DateTime(2022 , 04 , 18),
            Status = Enums.Status.Entregue,
            Salesman = new Salesman() {
                   SalesmanId = 1,
                   Cpf = "88888888888",
                   Email = "teste@test.com",
                   Name = "Testando",
                   Phone = "88888888888"
            },
            Cart = new() {
                new() {
                    ItemId = 1,
                    Amount = 10
                }
            }
        },
        new Sale {
            SaleId = new ObjectId("c08b8046d761a9e553eeafb6"),
            SalesDate = new DateTime(2022 , 04 , 01),
            Status = Enums.Status.Cancelado,
            Salesman = new Salesman() {
                   SalesmanId = 1,
                   Cpf = "88888888888",
                   Email = "teste@test.com",
                   Name = "Testando",
                   Phone = "88888888888"
            },
            Cart = new() {
                new() {
                    ItemId = 1,
                    Amount = 10
                }
            }
        },
        new Sale {
            SaleId = new ObjectId("76c78935c0a3a86a0989ef25"),
            SalesDate = new DateTime(2021 , 12 , 18),
            Status = Enums.Status.PagamentoAprovado,
            Salesman = new Salesman() {
                   SalesmanId = 1,
                   Cpf = "88888888888",
                   Email = "teste@test.com",
                   Name = "Testando",
                   Phone = "88888888888"
            },
            Cart = new() {
                new() {
                    ItemId = 1,
                    Amount = 10
                }
            }
        },
        new Sale {
            SaleId = new ObjectId("5362352564045bd6558e997c"),
            SalesDate = new DateTime(2022 , 06 , 09),
            Status = Enums.Status.EnviadoParaTransportadora,
            Salesman = new Salesman() {
                   SalesmanId = 1,
                   Cpf = "88888888888",
                   Email = "teste@test.com",
                   Name = "Testando",
                   Phone = "88888888888"
            },
            Cart = new() {
                new() {
                    ItemId = 1,
                    Amount = 10
                }
            }
        },
        new Sale {
            SaleId = new ObjectId("507f1f55bcf96cd799438110"),
            SalesDate = new DateTime(2022,08,31),
            Status = Enums.Status.AguardandoPagamento,
            Salesman = new Salesman() {
                   SalesmanId = 1,
                   Cpf = "88888888888",
                   Email = "teste@test.com",
                   Name = "Testando",
                   Phone = "88888888888"
            },
            Cart = new() {
                new() {
                    ItemId = 1,
                    Amount = 10
                }
            }
        },
        };
    }
}