﻿using PaymentApi.Services.Models;

namespace PaymentApi.Services.Fixtures;

public static class SalesmanFixture {
    public static Salesman Salesman => new Salesman() {
        Name = "Yuri",
        Cpf = "88888888888",
        Email = "ycss.v1@gmail.com",
        Phone = "+5571986955209",
        SalesmanId = 111111
    };
}
