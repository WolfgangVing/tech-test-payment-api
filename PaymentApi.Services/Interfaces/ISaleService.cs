using LiteDB;
using PaymentApi.Services.Enums;
using PaymentApi.Services.Models;

namespace PaymentApi.Services.Interfaces;

public interface ISaleService<T> {
    public Task<Sale> GetSaleByIdService(string id);
    public Task<Sale> PostSaleService(Salesman salesman, List<Item> cart);
    public Task<bool> PutSaleService(Sale sale);
}
