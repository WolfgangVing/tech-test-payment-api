﻿# Management Payment API
<p align="center">
    <img alt="language: csharp" src="https://img.shields.io/badge/Language-csharp-blueviolet">
    <img alt="Plataforma .Net-Core 6" src="https://img.shields.io/badge/Plataforma-.Net--Core%206-blueviolet">
    <img alt="Lite Database" src="https://img.shields.io/badge/DB-LiteDatabase-orange">
</p>

## About:
This is an challenger code for Pottential's Developer Bootcamp program which is to create a api that will manage
sales as a 3rd party, like a bridge between the salesman/retainer and the delivery service/company, 
so rather than managing sales itself, should be more accurate to say it's manage the status of it, 
by creating a new ticket(should we say a Sale's ID). For this challenge it's was required only three endpoints, GET, POST and PUT. Because this WebAPI manage the status only, I thought that NoSQL database could be a good approach.


## Libraries
With the addition of .Net-core 6 it was used some open-source libraries for tests and use of embedded database.

- [LiteDatabase](https://www.litedb.org/) - Embedded NoSQL database for .NET
- [Fluent Assertions](https://fluentassertions.com/) - For better assertion during TDD.
- [Moq](https://github.com/Moq/moq4/wiki/Quickstart) - Used for mocking the independency injection(there is more uses)
- [xUnit](https://xunit.net/)- Developer Testing Framework.

## Setup
It's required to have at least .NET core 6 installed.

1. Clone or download this project.
`git clone https://gitlab.com/WolfgangVing/tech-test-payment-api.git`

2. Open the project's api.
`cd .\tech-test-payment-api\PaymentApi.API\`

3. Build and Run the project's api.
`dotnet run`

4. Copy your listening port(normally 7274 or 5109) and paste at the URL's bar of your browser adding `/swagger/index.html`.
`http://localhost:5109/swagger/index.html`

## Using the API
It's necessary to pay attention to some rules should you not wanting to see BadRequest or NotFound status response.

The three endpoints are described below.

It's prepared 5 sale's data as sample, those sale's id is below for experimenting.
1. 646c7ca1e25b9f68cb5ef4e3
2. c08b8046d761a9e553eeafb6
3. 76c78935c0a3a86a0989ef25
4. 5362352564045bd6558e997c
5. 507f1f55bcf96cd799438110
---
### Get Sale By Id.
Will return a sale filtered by id.

#### Request
 `GET /Sale/api/:id`
 | parameter | type |
| --- | ---|
|id|string|

#### Response
In case it's a 200 OK status, would return a json data in the response body.
```json
{
  "vendaId": "string",
  "vendedor": {
    "salesmanId": 1,
    "name": "string",
    "cpf": "string",
    "email": "string",
    "phone": "string"
  },
  "carrinho": [
    {
      "itemId": 1,
      "amount": 1
    }
  ],
  "dataDaVenda": "dd/MM/yyyy - HH:mm:ss",
  "status": "string"
}
```
---
### Post A New Sales

Should be perform a post of a new sale, it's needed to pass a body with salesman's and cart's data, since it's required and would return a bad request nonetheless.
**Importante** Cart's should have at least one item, and each item should have a amount higher then 0, otherwise result in badrequest.

#### Request
 `POST /Sale/api/`

body:
```json
{
  "salesman": {
    "salesmanId": 0,
    "name": "string",
    "cpf": "string",
    "email": "string",
    "phone": "string"
  },
  "cart": [
    {
      "itemId": 0,
      "amount": 0
    }
  ]
}
```
#### Response
In case it's a 201 OK, would return a json data in the body response.
```json
{
  "vendaId": "6372cf4eb1cee00d2ff61427",
  "vendedor": {
    "salesmanId": 0,
    "name": "string",
    "cpf": "string",
    "email": "string",
    "phone": "string"
  },
  "carrinho": [
    {
      "itemId": 1,
      "amount": 1
    }
  ],
  "dataDaVenda": "14/11/2022 - 20:29:18",
  "status": "AguardandoPagamento"
}
```
---
### PUT a Sale
Will update the sale's status, based in numeral below.
| Status | Numeral|
|--------|--------|
| Cancelado | 0 |
|Aguardando Pagamento| 1 |
|Pagamento Aprovado| 2 |
|Enviando para transportadora| 3 |
|Entregue| 4 |

#### Request

`PUT /Sale/api/:id/:status`
| parameter | type |
| --- | ---|
|id|string|
|status| int|
#### Response
```json
{
  "vendaId": "6372cf4eb1cee00d2ff61427",
  "vendedor": {
    "salesmanId": 0,
    "name": "string",
    "cpf": "string",
    "email": "string",
    "phone": "string"
  },
  "carrinho": [
    {
      "itemId": 1,
      "amount": 1
    }
  ],
  "dataDaVenda": "14/11/2022 - 20:29:18",
  "status": "Status Changed"
}
```


What those words mean in english.
1. Cancelado = Cancelled
2.  Aguardando Pagamento = Awaiting payment
3. Pagamento Aprovado = Payment accepted
4. Enviando para transportadora = Shipping to Carrier
5. Entregue = Delivered

The update can only follow this rule:
1 -> 2 -> 3 -> 4
AND
1 -> 0 OR 2 -> 0

----

